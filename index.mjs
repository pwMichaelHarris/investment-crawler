////////////////////////////////////////////////////////////////////////////////
//  Examples
//    node index.mjs FHLSX FTBFX FXNAX

////////////////////////////////////////////////////////////////////////////////
//  Globals
const outFolder = ".";
const timeFactor = 1000;

////////////////////////////////////////////////////////////////////////////////
//  Let's the process sleep for a bit (done via a delayed promise resolving)
const sleep = (units) => new Promise(resolve => {
  setTimeout(resolve, units*timeFactor);
});

////////////////////////////////////////////////////////////////////////////////
//  getPage:
//    returns the text from morningstar.com for the company with the 
//    *companyCode*
import { request } from "node:https";

////////////////////////////////////////
//  The request options
const options = {
  "headers": {
    "authority": "www.morningstar.com",
    "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8",
    "accept-language": "en-US,en;q=0.6",
    "cache-control": "no-cache",
    "pragma": "no-cache",
    "sec-fetch-dest": "document",
    "sec-fetch-mode": "navigate",
    "sec-fetch-site": "same-origin",
    "sec-fetch-user": "?1",
    "sec-gpc": "1",
    "upgrade-insecure-requests": "1"
  },
  "referrerPolicy": "no-referrer-when-downgrade",
  "body": null,
  "method": "GET",
  "mode": "cors",
  "credentials": "include"
};

////////////////////////////////////////
//  The request options
const getPage = (companyCode) => new Promise((resolve, reject) => {
  const url = `https://www.morningstar.com/funds/xnas/${companyCode.toLowerCase()}/quote`;
  const req = request(url, options, (res) => {
    const { statusCode, headers } = res;
    
    if (statusCode / 100 !== 2) {
      const error = {
        code: "http-error",
        reason: "Unable to complete HTTP request",
        statusCode,
        toString: () => JSON.stringify(error),
      };
      reject(error);
    }
    
    const buffer = [];
    res.on("data", buffer.push.bind(buffer));
    res.on("end", () => resolve(Buffer.concat(buffer).toString("utf-8")));
    res.on("error", reject);
  });
  req.end();
});

////////////////////////////////////////////////////////////////////////////////
//  Returns a formatted date (YY-MM-DD) from a date object
const dateToDateString = (date) => {
  const year = date.getFullYear().toString().slice(2);
  const month = (date.getMonth() + 1).toString().padStart(2, "0");
  const day = (date.getDate()).toString().padStart(2, "0");
  
  return [year, month, day].join("-");
};

////////////////////////////////////////////////////////////////////////////////
//  Extracts the relevent values from the page text
const parsePage = (pageText) => {
  const value = /fund-quote__value.*?class="mdc-data-point mdc-data-point--number"[^>]*>([^<]+)/.exec(pageText)[1];
  const dateInput = /(\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}-\d{2}:\d{2})/g.exec(pageText)[1];
  const dateDate = new Date(dateInput);
  const dateOutput = dateToDateString(dateDate);
  
  return { value, date: dateOutput, };
};

////////////////////////////////////////////////////////////////////////////////
//  Safely open a new output file for writing with the name of 
//    "stock-market-YY-MM-DD-C.csv" where YY is the last two digits of the year;
//    MM the month (zero padded), DD is date (zero padded), and C is an 
//    optional, additional counter to ensure the file name is unique.
import { open } from "node:fs/promises";
const fileDate = new Date();
let countString="";
let count = 0;
let tryAgain = true;
let outFile;
while (outFile === undefined) {
  try {
    outFile = await open(`${outFolder}/stock-market-${dateToDateString(fileDate)}${countString}.csv`, "wx")
  } catch (err) {
    if (err?.code === "EEXIST") {
      count = count += 1;
      countString = `-${count}`;
    }
    else {
      throw err;
    }
  }
}

let outErrorFile;

////////////////////////////////////////////////////////////////////////////////
//  For command line given names, get the page and save the current day's value 
//    and when the page was last updated.
const names = process.argv.slice(2);
for (let name of names) {
  try {
    const pageText = await getPage(name);
    const {date, value} = await parsePage(pageText);
    console.log([date, name, value].join(","));
    await outFile.write([date, name, value].join(",") + "\n");
  }
  
  catch (err) {
    outErrorFile = outErrorFile ?? (await open(`${outFolder}/stock-market-${dateToDateString(fileDate)}-error.txt`, "a"));
    console.error({date: (new Date()).toString(), name, err: err.toString()});
    outErrorFile.write(JSON.stringify({name, err: err.toString()}) + "\n");
    await outFile.write(["ERROR", name, "ERROR"].join(",") + "\n");
  }
  
  await sleep(Math.random()*5+10);
}

////////////////////////////////////////
await outFile.close();
if (outErrorFile) {
  await outErrorFile.close();
}

